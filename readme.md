# time-pool
This is a command line tool to keep, in a file, the time spent doing one or more tasks.

The amount is added into a file (min = `-(2⁶³)` max = `2⁶³−1`), converted to minute.


## Examples

### Add 5 weeks and remove 3 days, and print between the operations
```
time-pool --print --weeks 5 --print --days -3 --print
```

**Output**
```
0 minutes in the pool
50400 minutes in the pool
46080 minutes in the pool
```