mod components;

use crate::components::MinuteToDayResult;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::PathBuf;
use std::{env, fs, process};

const POOL_FILE_NAME: &str = "./time.pool";
const HELP_PARAMETER: &str = "--help";
const DOCS_PARAMETER: &str = "--docs";
const PRINT_PARAMETER: &str = "--print";
const PRINT_AS_HOURS_PARAMETER: &str = "--print-hour";
const PRINT_AS_DAY_PARAMETER: &str = "--print-day";
const PRINT_AS_WEEK_PARAMETER: &str = "--print-week";
const WEEKS_PARAMETER: &str = "--week";
const DAYS_PARAMETER: &str = "--day";
const HOURS_PARAMETER: &str = "--hour";
const MINUTES_PARAMETER: &str = "--minute";
const FILE_PARAMETER: &str = "--file";
const ZERO_STR: &str = "0";

fn main() {
    let parameters: Vec<(String, Option<String>)> = fetch_application_params(env::args().collect());
    let file: PathBuf = get_file(&parameters);

    if parameters.is_empty() {
        print_help();
        process::exit(-1);
    } else if contains_parameter(HELP_PARAMETER, &parameters) {
        print_help();
        return;
    } else if contains_parameter(DOCS_PARAMETER, &parameters) {
        print_documentation();
        return;
    }

    let mut minutes_from_pool: i64 = read_pool_file_as_integer(&file);

    for (parameter, optional_value) in parameters {
        if PRINT_PARAMETER.eq_ignore_ascii_case(&parameter) {
            println!("{} minute(s)", minutes_from_pool);
            continue;
        } else if PRINT_AS_HOURS_PARAMETER.eq_ignore_ascii_case(&parameter) {
            let hours: f64 = convert_minutes_to_hours(minutes_from_pool);
            let decimal: i64 = hours.trunc() as i64;
            let factional: f64 = hours.fract();

            if decimal != 0 && factional != 0.0 {
                println!("{} hour(s) and {:.0} minute(s)", decimal, factional * 60.0);
            } else if decimal != 0 && factional == 0.0 {
                println!("{} hour(s)", hours);
            } else {
                println!("0 hour(s) and {:.0} minute(s)", factional * 60.0);
            }

            continue;
        } else if PRINT_AS_DAY_PARAMETER.eq_ignore_ascii_case(&parameter) {
            let mut override_of_number_of_hours_per_days: Option<u8> = None;

            if let Some(hours_per_day_str) = optional_value {
                if let Ok(hours_per_day) = u8::from_str_radix(&hours_per_day_str, 10) {
                    if hours_per_day > 24 {
                        override_of_number_of_hours_per_days = Some(24);
                    } else {
                        override_of_number_of_hours_per_days = Some(hours_per_day);
                    }
                }
            }

            let result: MinuteToDayResult = convert_minutes_to_days(minutes_from_pool, override_of_number_of_hours_per_days);
            let days: i64 = result.days();
            let minutes: i64 = result.minutes_left();

            println!("{} day(s) and {} minute(s)", days, minutes);
            continue;
        } else if PRINT_AS_WEEK_PARAMETER.eq_ignore_ascii_case(&parameter) {
            println!("{:.2} week(s)", convert_minutes_to_weeks(minutes_from_pool));
            continue;
        }

        let minute_as_int: i64;
        if let Some(value) = optional_value {
            minute_as_int = convert_str_to_sixty_four_integer(&value);
        } else {
            minute_as_int = 0;
        }

        minutes_from_pool += convert_to_minutes(&parameter, minute_as_int);
    }

    overwrite_pool_file_with_minutes(minutes_from_pool, file);
}

fn get_file(parameters: &Vec<(String, Option<String>)>) -> PathBuf {
    for (parameter, value) in parameters {
        if parameter.eq(FILE_PARAMETER) {
            if let Some(path) = value {
                return PathBuf::from(path);
            }
        }
    }

    return PathBuf::from(&POOL_FILE_NAME);
}

fn print_help() {
    println!("Usage:\t\ttime-pool [OPTIONS...]\n");
    println!("\t{}, print this help.", HELP_PARAMETER);
    println!("\t{}, print the documentation.", DOCS_PARAMETER);
    println!("\t{}, print the current amount (minutes) in the pool.", PRINT_PARAMETER);
    println!("\t{}, print the current amount (hours) in the pool.", PRINT_AS_HOURS_PARAMETER);
    println!("\t{}, print the current amount (days) in the pool.", PRINT_AS_DAY_PARAMETER);
    println!("\t{}, print the current amount (week) in the pool.", PRINT_AS_WEEK_PARAMETER);
    println!("\t{}, choose the file that contains the time.", FILE_PARAMETER);

    println!("\nParameters that edit the pool");
    println!("\t Those parameters need an integer argument (negative or positive) ");
    println!("\t\t{}", WEEKS_PARAMETER);
    println!("\t\t{}", DAYS_PARAMETER);
    println!("\t\t{}", HOURS_PARAMETER);
    println!("\t\t{}", MINUTES_PARAMETER);
    println!("\t Examples:");
    println!("\t\t\"{0} -{1}\" will REMOVE {1} weeks from the pool", WEEKS_PARAMETER, 5);
    println!("\t\t\"{0} {1}\" will ADD {1} days from the pool", DAYS_PARAMETER, 150);

    println!("\nComplete examples:");
    println!("\t time-pool --print {WEEKS_PARAMETER} 5 {DAYS_PARAMETER} -2");
    println!("\t time-pool --print");
}

fn print_documentation() {
    println!("Documentation for: time-pool");
    println!("\t Pool range: {} to {} (64bit signed integer)", i64::MIN, i64::MAX);
}

/// A function to check if the specified parameter vec contains the parameter
fn contains_parameter(wanted_parameter: &str, parameters: &Vec<(String, Option<String>)>) -> bool {
    for (parameter, _value) in parameters {
        if *(parameter) == wanted_parameter {
            return true;
        }
    }

    return false;
}

fn overwrite_pool_file_with_minutes(minutes_from_pool: i64, file: PathBuf) {
    if let Some(raw_file_name) = &file.file_name() {
        if let Some(file_name) = raw_file_name.to_str() {
            let mut temp_file: PathBuf = file.clone();
            let tmp_file_name = String::from(file_name) + ".tmp";
            temp_file.set_file_name(&tmp_file_name);

            if let Ok(tmp_pool_file) = File::create(&temp_file) {
                BufWriter::new(tmp_pool_file)
                    .write(minutes_from_pool.to_string().as_bytes())
                    .expect("Unable to write to the temp file!");

                fs::remove_file(&file).expect(&format!("Unable to remove the temp file to `{}`", file_name));
                fs::rename(&temp_file, &file).expect(&format!("Unable to rename the temp file to `{}`", file_name));
            } else {
                panic!("Unable to create the temp file '{}' !", &tmp_file_name);
            }
        }
    } else {
        panic!("Unable to fetch the file name of the specified file !");
    }
}

fn read_pool_file_as_integer(file_path: &PathBuf) -> i64 {
    return fs::read_to_string(file_path)
        .unwrap_or(String::from(ZERO_STR))
        .parse::<i64>()
        .unwrap_or(0);
}

/// A function that convert the value (integer) to minutes, based on the parameter (week, days, hours, ect)
/// ## The parameter
/// * Return 0, if the parameter is not defined
/// ## The value
/// * Return 0 in case the value is not an integer
fn convert_to_minutes(parameter: &str, integer: i64) -> i64 {
    if WEEKS_PARAMETER.eq(parameter) {
        return 10080 * integer;
    } else if DAYS_PARAMETER.eq(parameter) {
        return 1440 * integer;
    } else if HOURS_PARAMETER.eq(parameter) {
        return 60 * integer;
    } else if MINUTES_PARAMETER.eq(parameter) {
        return integer;
    } else {
        return 0;
    }
}

/// A function that convert minutes to weeks
/// ## The minutes
/// * Return the converted value
fn convert_minutes_to_weeks(minutes: i64) -> f64 {
    if minutes == 0 {
        return 0.0;
    } else {
        return minutes as f64 / 10080.0;
    }
}

/// A function that convert minutes to days
/// ## The minutes
/// * Return the converted value
fn convert_minutes_to_days(minutes: i64, override_of_number_of_hours_per_days: Option<u8>) -> MinuteToDayResult {
    if minutes == 0 {
        return MinuteToDayResult::empty();
    } else {
        if let Some(hours_per_days) = override_of_number_of_hours_per_days {
            if let Some(minutes_in_day) = (hours_per_days as u16).checked_mul(60) {
                return build_minute_to_day(minutes, minutes_in_day);
            }
        }

        return build_minute_to_day(minutes, 1440);
    }
}

fn build_minute_to_day(minutes: i64, minutes_in_day: u16) -> MinuteToDayResult {
    let number_of_days: f64 = minutes as f64 / minutes_in_day as f64;
    let decimal: i64 = number_of_days.trunc() as i64;
    let minutes_left: i64 = (number_of_days.fract() * minutes_in_day as f64) as i64;
    return MinuteToDayResult::new(decimal, minutes_left);
}

/// A function that convert minutes to hours
/// ## The minutes
/// * Return the converted value
fn convert_minutes_to_hours(minutes: i64) -> f64 {
    if minutes == 0 {
        return 0.0;
    } else {
        return minutes as f64 / 60.0;
    }
}

/// A function that convert the str to a 64 bit integer
/// ## The string to convert
/// * Return the converted value, or zero if unable to convert
fn convert_str_to_sixty_four_integer(str_to_convert: &str) -> i64 {
    if let Ok(converted) = str_to_convert.parse::<i64>() {
        return converted;
    } else {
        return 0;
    }
}

/// Method to bind the value to the parameter
fn fetch_application_params(parameter_arguments: Vec<String>) -> Vec<(String, Option<String>)> {
    let mut values: Vec<(String, Option<String>)> = Vec::new();
    let number_of_arguments: usize = parameter_arguments.len();

    if number_of_arguments > 1 {
        for argument_index in 1..number_of_arguments {
            let current_argument: &str = parameter_arguments.get(argument_index).unwrap();

            if FILE_PARAMETER.eq_ignore_ascii_case(current_argument) {
                let value: String = String::from(
                    parameter_arguments
                        .get(argument_index + 1)
                        .expect("Unable to read the value of the parameter 'file'!"),
                );
                values.push((String::from(FILE_PARAMETER), Some(value)));
            } else if WEEKS_PARAMETER.eq_ignore_ascii_case(current_argument) {
                let value: String = String::from(
                    parameter_arguments
                        .get(argument_index + 1)
                        .expect("Unable to read the value of the parameter 'weeks'!"),
                );
                values.push((String::from(WEEKS_PARAMETER), Some(value)));
            } else if DAYS_PARAMETER.eq_ignore_ascii_case(current_argument) {
                let value: String = String::from(
                    parameter_arguments
                        .get(argument_index + 1)
                        .expect("Unable to read the value of the parameter 'days'!"),
                );
                values.push((String::from(DAYS_PARAMETER), Some(value)));
            } else if HOURS_PARAMETER.eq_ignore_ascii_case(current_argument) {
                let value: String = String::from(
                    parameter_arguments
                        .get(argument_index + 1)
                        .expect("Unable to read the value of the parameter 'hours'!"),
                );
                values.push((String::from(HOURS_PARAMETER), Some(value)));
            } else if MINUTES_PARAMETER.eq_ignore_ascii_case(current_argument) {
                let value: String = String::from(
                    parameter_arguments
                        .get(argument_index + 1)
                        .expect("Unable to read the value of the parameter 'minutes'!"),
                );
                values.push((String::from(MINUTES_PARAMETER), Some(value)));
            } else if PRINT_PARAMETER.eq_ignore_ascii_case(current_argument) {
                values.push((String::from(PRINT_PARAMETER), None));
            } else if PRINT_AS_HOURS_PARAMETER.eq_ignore_ascii_case(current_argument) {
                values.push((String::from(PRINT_AS_HOURS_PARAMETER), None));
            } else if PRINT_AS_DAY_PARAMETER.eq_ignore_ascii_case(current_argument) {
                if let Some(parameter) = parameter_arguments.get(argument_index + 1) {
                    values.push((String::from(PRINT_AS_DAY_PARAMETER), Some(parameter.clone())));
                } else {
                    values.push((String::from(PRINT_AS_DAY_PARAMETER), None));
                }
            } else if PRINT_AS_WEEK_PARAMETER.eq_ignore_ascii_case(current_argument) {
                values.push((String::from(PRINT_AS_WEEK_PARAMETER), None));
            } else if DOCS_PARAMETER.eq_ignore_ascii_case(current_argument) {
                values.push((String::from(DOCS_PARAMETER), None));
            }
        }
    }

    return values;
}

#[cfg(test)]
mod convert_minutes_to {
    use super::*;

    #[test]
    fn convert_minutes_to_weeks_zero() {
        // given
        let minutes: i64 = 0;

        // when
        let to_assert: f64 = convert_minutes_to_weeks(minutes);

        // then
        assert_eq!(to_assert, 0.0);
    }

    #[test]
    fn convert_minutes_to_weeks_test() {
        // given
        let minutes: i64 = 60;

        // when
        let to_assert: f64 = convert_minutes_to_weeks(minutes);

        // then
        assert_eq!(to_assert, 0.0059523809523809521);
    }

    #[test]
    fn convert_minutes_to_days_no_override_minutes_only_test() {
        // given
        let minutes: i64 = 60;

        // when
        let to_assert: MinuteToDayResult = convert_minutes_to_days(minutes, None);
        let minute_left_to_assert: i64 = to_assert.minutes_left();
        let days_to_assert: i64 = to_assert.days();

        // then
        assert_eq!(minute_left_to_assert, 60);
        assert_eq!(days_to_assert, 0);
    }

    #[test]
    fn convert_minutes_to_days_no_override_minutes_and_days_test() {
        // given
        let minutes: i64 = 1445454;

        // when
        let to_assert: MinuteToDayResult = convert_minutes_to_days(minutes, None);
        let minute_left_to_assert: i64 = to_assert.minutes_left();
        let days_to_assert: i64 = to_assert.days();

        // then
        assert_eq!(minute_left_to_assert, 1134);
        assert_eq!(days_to_assert, 1003);
    }

    #[test]
    fn convert_minutes_to_days_no_7hours_override_test() {
        // given
        let minutes: i64 = 420;

        // when
        let to_assert: MinuteToDayResult = convert_minutes_to_days(minutes, Some(7));
        let minute_left_to_assert: i64 = to_assert.minutes_left();
        let days_to_assert: i64 = to_assert.days();

        // then
        assert_eq!(days_to_assert, 1);
        assert_eq!(minute_left_to_assert, 0);
    }

    #[test]
    fn convert_minutes_to_hour_test() {
        // given
        let minutes: i64 = 60;

        // when
        let to_assert: f64 = convert_minutes_to_hours(minutes);

        // then
        assert_eq!(to_assert, 1.0);
    }
}

#[cfg(test)]
mod convert_str_to_integer {
    use super::*;

    #[test]
    fn convert_str_to_integer_invalid() {
        // given
        let value: &str = "asdufgha8s7";

        // when
        let to_assert: i64 = convert_str_to_sixty_four_integer(value);

        // then
        assert_eq!(to_assert, 0);
    }

    #[test]
    fn convert_str_to_integer_negative() {
        // given
        let value: &str = "-5154454";

        // when
        let to_assert: i64 = convert_str_to_sixty_four_integer(value);

        // then
        assert_eq!(to_assert, -5154454);
    }

    #[test]
    fn convert_str_to_integer_positive() {
        // given
        let value: &str = "5154454";

        // when
        let to_assert: i64 = convert_str_to_sixty_four_integer(value);

        // then
        assert_eq!(to_assert, 5154454);
    }

    #[test]
    fn convert_str_to_integer_positive_zero() {
        // given
        let value: &str = "0";

        // when
        let to_assert: i64 = convert_str_to_sixty_four_integer(value);

        // then
        assert_eq!(to_assert, 0);
    }

    #[test]
    fn convert_str_to_integer_negative_zero() {
        // given
        let value: &str = "-0";

        // when
        let to_assert: i64 = convert_str_to_sixty_four_integer(value);

        // then
        assert_eq!(to_assert, 0);
    }
}

#[cfg(test)]
mod fetch_application_params {
    use super::*;

    #[test]
    fn fetch_application_params_empty() {
        // given
        let parameters: Vec<String> = Vec::new();

        // when
        let to_assert: Vec<(String, Option<String>)> = fetch_application_params(parameters);

        // then
        assert_eq!(to_assert.is_empty(), true);
    }

    #[test]
    fn fetch_application_params_print() {
        // given
        let mut parameters: Vec<String> = Vec::new();
        parameters.push(String::from("super_application_path!"));
        parameters.push(String::from(PRINT_PARAMETER));

        // when
        let option: Vec<(String, Option<String>)> = fetch_application_params(parameters);

        // then
        assert_eq!(option, vec![(String::from(PRINT_PARAMETER), None)]);
    }

    #[test]
    fn fetch_application_params_days_print() {
        // given
        let mut parameters: Vec<String> = Vec::new();
        parameters.push(String::from("super_application_path!"));
        parameters.push(String::from(DAYS_PARAMETER));
        parameters.push(String::from(String::from("-150")));
        parameters.push(String::from(PRINT_PARAMETER));

        // when
        let option: Vec<(String, Option<String>)> = fetch_application_params(parameters);

        // then
        assert_eq!(
            option,
            vec![
                (String::from(DAYS_PARAMETER), Some(String::from("-150"))),
                (String::from(PRINT_PARAMETER), None),
            ]
        );
    }

    #[test]
    fn fetch_application_params_days_print_week() {
        // given
        let mut parameters: Vec<String> = Vec::new();
        parameters.push(String::from("super_application_path!"));
        parameters.push(String::from(DAYS_PARAMETER));
        parameters.push(String::from(String::from("-150")));
        parameters.push(String::from(PRINT_PARAMETER));
        parameters.push(String::from(WEEKS_PARAMETER));
        parameters.push(String::from(String::from("550")));

        // when
        let option: Vec<(String, Option<String>)> = fetch_application_params(parameters);

        // then
        assert_eq!(
            option,
            vec![
                (String::from(DAYS_PARAMETER), Some(String::from("-150"))),
                (String::from(PRINT_PARAMETER), None),
                (String::from(WEEKS_PARAMETER), Some(String::from("550"))),
            ]
        );
    }
}

#[cfg(test)]
mod convert_to_minutes_weeks {
    use super::*;
    use rand::Rng;

    #[test]
    fn convert_to_minutes_weeks_zero() {
        // given
        let value: i64 = 0;

        // when
        let to_assert: i64 = convert_to_minutes(WEEKS_PARAMETER, value);

        // then
        assert_eq!(to_assert, 0);
    }

    #[test]
    fn convert_to_minutes_weeks_random() {
        // given
        let random_number: i64 = rand::thread_rng().gen_range(-2000..2000);

        // when / then
        if random_number != 0 {
            let to_assert: i64 = convert_to_minutes(WEEKS_PARAMETER, random_number);
            assert_eq!(to_assert, random_number * 7 * 24 * 60);
        } else {
            let to_assert: i64 = convert_to_minutes(WEEKS_PARAMETER, random_number);
            assert_eq!(to_assert, 0);
        }
    }

    #[test]
    fn convert_to_minutes_weeks_positive() {
        // given
        let value: i64 = 36;

        // when
        let to_assert: i64 = convert_to_minutes(WEEKS_PARAMETER, value);

        // then
        assert_eq!(to_assert, 362880);
    }

    #[test]
    fn convert_to_minutes_weeks_negative() {
        // given
        let value: i64 = -36;

        // when
        let to_assert: i64 = convert_to_minutes(WEEKS_PARAMETER, value);

        // then
        assert_eq!(to_assert, -362880);
    }
}

#[cfg(test)]
mod convert_to_minutes_days {
    use super::*;
    use rand::Rng;

    #[test]
    fn convert_to_minutes_days_zero() {
        // given
        let value: i64 = 0;

        // when
        let to_assert: i64 = convert_to_minutes(DAYS_PARAMETER, value);

        // then
        assert_eq!(to_assert, 0);
    }

    #[test]
    fn convert_to_minutes_days_random() {
        // given
        let random_number: i64 = rand::thread_rng().gen_range(-2000..2000);

        // when / then
        if random_number != 0 {
            let to_assert: i64 = convert_to_minutes(DAYS_PARAMETER, random_number);
            assert_eq!(to_assert, random_number * 24 * 60);
        } else {
            let to_assert: i64 = convert_to_minutes(DAYS_PARAMETER, random_number);
            assert_eq!(to_assert, 0);
        }
    }

    #[test]
    fn convert_to_minutes_days_positive() {
        // given
        let value: i64 = 36;

        // when
        let to_assert: i64 = convert_to_minutes(DAYS_PARAMETER, value);

        // then
        assert_eq!(to_assert, 51840);
    }

    #[test]
    fn convert_to_minutes_days_negative() {
        // given
        let value: i64 = -36;

        // when
        let to_assert: i64 = convert_to_minutes(DAYS_PARAMETER, value);

        // then
        assert_eq!(to_assert, -51840);
    }
}

#[cfg(test)]
mod convert_to_minutes_hours {
    use super::*;
    use rand::Rng;

    #[test]
    fn convert_to_minutes_hours_zero() {
        // given
        let value: i64 = 0;

        // when
        let to_assert: i64 = convert_to_minutes(HOURS_PARAMETER, value);

        // then
        assert_eq!(to_assert, 0);
    }

    #[test]
    fn convert_to_minutes_hours_random() {
        // given
        let random_number: i64 = rand::thread_rng().gen_range(-2000..2000);

        // when / then
        if random_number != 0 {
            let to_assert: i64 = convert_to_minutes(HOURS_PARAMETER, random_number);
            assert_eq!(to_assert, random_number * 60);
        } else {
            let to_assert: i64 = convert_to_minutes(HOURS_PARAMETER, random_number);
            assert_eq!(to_assert, 0);
        }
    }

    #[test]
    fn convert_to_minutes_hours_positive() {
        // given
        let value: i64 = 36;

        // when
        let to_assert: i64 = convert_to_minutes(HOURS_PARAMETER, value);

        // then
        assert_eq!(to_assert, 2160);
    }

    #[test]
    fn convert_to_minutes_hours_negative() {
        // given
        let value: i64 = -36;

        // when
        let to_assert: i64 = convert_to_minutes(HOURS_PARAMETER, value);

        // then
        assert_eq!(to_assert, -2160);
    }
}

#[cfg(test)]
mod convert_to_minutes_minutes {
    use super::*;
    use rand::Rng;

    #[test]
    fn convert_to_minutes_minutes_zero() {
        // given
        let value: i64 = 0;

        // when
        let to_assert: i64 = convert_to_minutes(MINUTES_PARAMETER, value);

        // then
        assert_eq!(to_assert, 0);
    }

    #[test]
    fn convert_to_minutes_minutes_random() {
        // given
        let random_number: i64 = rand::thread_rng().gen_range(-2000..2000);

        // when / then
        if random_number != 0 {
            let to_assert: i64 = convert_to_minutes(MINUTES_PARAMETER, random_number);
            assert_eq!(to_assert, random_number);
        } else {
            let to_assert: i64 = convert_to_minutes(MINUTES_PARAMETER, random_number);
            assert_eq!(to_assert, 0);
        }
    }

    #[test]
    fn convert_to_minutes_minutes_positive() {
        // given
        let value: i64 = 36;

        // when
        let to_assert: i64 = convert_to_minutes(MINUTES_PARAMETER, value);

        // then
        assert_eq!(to_assert, 36);
    }

    #[test]
    fn convert_to_minutes_minutes_negative() {
        // given
        let value: i64 = -36;

        // when
        let to_assert: i64 = convert_to_minutes(MINUTES_PARAMETER, value);

        // then
        assert_eq!(to_assert, -36);
    }
}
