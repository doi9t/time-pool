#[derive(Debug, Clone)]
pub struct MinuteToDayResult {
    days: i64,
    minutes_left: i64,
}

impl MinuteToDayResult {
    pub fn empty() -> MinuteToDayResult {
        return MinuteToDayResult { days: 0, minutes_left: 0 };
    }

    pub fn new(days: i64, minutes_left: i64) -> MinuteToDayResult {
        return MinuteToDayResult { days, minutes_left };
    }

    pub fn days(&self) -> i64 {
        return self.days;
    }

    pub fn minutes_left(&self) -> i64 {
        return self.minutes_left;
    }
}
